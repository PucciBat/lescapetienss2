'use strict';

module.exports = function(app) {
    app.get('/', function(req, res) {
        res.render('pages/home');
    });

    app.get('/hugues-capet', function (req, res) {
        res.render('pages/hugues-capet');
    });

    app.get('/philippe-auguste', function (req, res) {
        res.render('pages/philippe-auguste');
    });

    app.get('/louis-ix', function (req, res) {
        res.render('pages/louis-ix');
    });

    app.get('/philippe-le-bel', function (req, res) {
        res.render('pages/philippe-le-bel');
    });

    app.get('/form', function (req, res) {
        res.render('pages/form');
    });

    app.get('/success', function (rq, res) {
        res.render('pages/success');
    });
};