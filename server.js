'use strict';

var express = require('express');
var routes = require('./routes/index.js');
var bodyParser = require('body-parser');
var fs = require('fs');
var port = process.env.PORT || 3000;

var app = express();

var urlencodedParser = bodyParser.urlencoded({extended: false});



app.post('/form', urlencodedParser, function(req, res) {
    console.log(req.body);
    res.render('pages/success', {data: req.body});
    var dataForm = JSON.stringify(req.body, null, 2);
    fs.appendFile('data.json', dataForm, (err) => {
        if (err) throw err;
    });
});

app.use('/public', express.static(process.cwd() + '/public'));
app.set('view engine', 'ejs');


routes(app);

app.listen(port, function() {
    console.log('Server listening on port ' + port + '...');
});