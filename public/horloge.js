
//Horloge
function date_heure(id)
{
        var myClock = document.getElementById(id);
        myClock.innerHTML = getClock(myClock);
        setInterval('date_heure("'+id+'");','1000');
        return true;
}

function changeColor (minute, elem){
        var yellow = "colorHorlogeYellow";
        var blue = "colorHorlogeBlue";
        if ( (minute % 2) == 0) {
                elem.classList.remove(blue);
                elem.classList.add(yellow);
        } 
        else {
                elem.classList.remove(yellow);
                elem.classList.add(blue);
        }
        first = false;
}

function getClock (myClock){
        var date = new Date;
        var h = date.getHours();
        if(h<10)
        {
                h = "0"+h;
        }
        var m = date.getMinutes();
        changeColor(m, myClock);
        if(m<10)
        {
                m = "0"+m;
        }
        var s = date.getSeconds();
        if(s<10)
        {
                s = "0"+s;
        }
        return h+':'+m+':'+s;
}